# Devires Framework Commons

Classes utilitárias de propósito geral para reuso em projetos.

### Publicar no Repositório Maven Local

> ./gradlew publishToMavenLocal

### Declarar Repositório Local nos Projetos para Usar

```
repositories {
    ...
    mavenLocal()
}
```

### Publicar no Repositório Maven do GitLab Devires

> ./gradlew publish