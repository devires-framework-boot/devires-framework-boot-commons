package br.com.devires.framework.boot.commons.openapi;

public class SecuritySchemeName {

    public static final String BEARER_TOKEN = "bearerToken";

    public static final String CLIENT_CREDENTIALS = "clientCredentials";

    public static final String AUTHORIZATION_CODE = "authorizationCode";

}
