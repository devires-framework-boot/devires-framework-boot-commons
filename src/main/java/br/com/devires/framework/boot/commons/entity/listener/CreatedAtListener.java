package br.com.devires.framework.boot.commons.entity.listener;

import jakarta.persistence.PrePersist;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CreatedAtListener {

    @PrePersist
    public void prePersistForCreateAuditInfo(final Creatable entity) {
        entity.setCreatedAt(new Date());
    }

}
