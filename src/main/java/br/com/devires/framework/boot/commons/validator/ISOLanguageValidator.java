package br.com.devires.framework.boot.commons.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class ISOLanguageValidator implements ConstraintValidator<ISOLanguage, CharSequence> {

    private List<String> languages;

    @Override
    public void initialize(ISOLanguage annotation) {
        this.languages = Arrays.asList(Locale.getISOLanguages());
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        return languages.contains(value);
    }

}
