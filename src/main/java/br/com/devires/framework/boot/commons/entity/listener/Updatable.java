package br.com.devires.framework.boot.commons.entity.listener;

import java.util.Date;

public interface Updatable {

    void setUpdatedAt(final Date updatedAt);

}
