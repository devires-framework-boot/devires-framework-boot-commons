package br.com.devires.framework.boot.commons.feign.decoder;

import br.com.devires.framework.boot.commons.exception.ModelException;
import br.com.devires.framework.boot.commons.model.CommonErrors;
import br.com.devires.framework.boot.commons.model.ErrorModel;
import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;

/* Do NOT use @Component */
public class ErrorModelDecoder implements ErrorDecoder {

    @Autowired
    private Decoder decoder;

    @Override
    @SneakyThrows
    public Exception decode(String methodKey, Response response) {
        try {
            ErrorModel errorModel = (ErrorModel) decoder.decode(response, ErrorModel.class);
            return new ModelException(errorModel);
        } catch (Exception e) {
            ErrorModel errorModel = CommonErrors.UNEXPECTED_ERROR.get();
            if (response != null && response.body() != null) {
                String payload = new String(response.body().asInputStream().readAllBytes(), StandardCharsets.UTF_8);
                errorModel.setDetails(payload);
            }
            return new ModelException(errorModel, e);
        }
    }

}
