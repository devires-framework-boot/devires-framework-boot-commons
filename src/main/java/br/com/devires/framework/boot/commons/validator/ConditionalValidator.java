package br.com.devires.framework.boot.commons.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConditionalValidator implements ConstraintValidator<Conditional, Object> {

    private String message;
    private String selected;
    private List<String> values;
    private String[] required;

    @Override
    public void initialize(Conditional annotation) {
        this.message = annotation.message();
        this.selected = annotation.selected();
        this.required = annotation.required();
        this.values = Stream.of(annotation.values()).map(String::toLowerCase).collect(Collectors.toList());
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Boolean isValid = true;
        BeanWrapper beanWrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);
        Object actualValue = beanWrapper.getPropertyValue(selected);
        if (!isEmpty(actualValue)) {
            if (values.contains(actualValue.toString().toLowerCase())) {
                for (String propName : required) {
                    Object requiredValue = beanWrapper.getPropertyValue(propName);
                    isValid = requiredValue != null && !isEmpty(requiredValue);
                    if (!isValid) {
                        context.disableDefaultConstraintViolation();
                        context.buildConstraintViolationWithTemplate(message).addPropertyNode(propName).addConstraintViolation();
                    }
                }
            }
        }
        return isValid;
    }

    private boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof String && obj.toString().trim().equals("")) {
            return true;
        }
        return false;
    }

}
