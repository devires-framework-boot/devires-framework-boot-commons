package br.com.devires.framework.boot.commons.redis;

import io.lettuce.core.RedisCommandTimeoutException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.data.redis.RedisConnectionFailureException;

@Slf4j
public class RedisCacheErrorHandler implements CacheErrorHandler {

    @Override
    public void handleCacheGetError(RuntimeException ex, Cache cache, Object key) {
        handleRedisExceptions(ex);
        log.error("Unable to get from cache " + cache.getName(), ex);
    }

    @Override
    public void handleCachePutError(RuntimeException ex, Cache cache, Object key, Object value) {
        handleRedisExceptions(ex);
        log.error("Unable to put into cache " + cache.getName(), ex);
    }

    @Override
    public void handleCacheEvictError(RuntimeException ex, Cache cache, Object key) {
        handleRedisExceptions(ex);
        log.error("Unable to evict from cache " + cache.getName(), ex);
    }

    @Override
    public void handleCacheClearError(RuntimeException ex, Cache cache) {
        handleRedisExceptions(ex);
        log.error("Unable to clean cache " + cache.getName(), ex);
    }

    /**
     * We handle redis availability exceptions, if the exception occurs
     * then it is treated as a cache miss and gets the data from actual storage
     */
    private void handleRedisExceptions(RuntimeException exception) {
        try {
            throw exception;
        } catch (RedisCommandTimeoutException | RedisConnectionFailureException e) {
            return;
        }
    }

}