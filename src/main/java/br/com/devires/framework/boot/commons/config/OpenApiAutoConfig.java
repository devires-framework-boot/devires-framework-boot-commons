package br.com.devires.framework.boot.commons.config;

import br.com.devires.framework.boot.commons.model.ErrorModel;
import br.com.devires.framework.boot.commons.openapi.DeviresExtensionConstants;
import br.com.devires.framework.boot.commons.openapi.SecuritySchemeName;
import br.com.devires.framework.boot.commons.servlet.SwaggerUICorsProxyServlet;
import br.com.devires.framework.boot.commons.utils.ResourceUtils;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.*;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServlet;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;

@Configuration
@ConditionalOnClass(OpenAPI.class)
public class OpenApiAutoConfig {

    private static final String PROXY_SERVLET_URI = "/swagger-ui/proxy";

    @Autowired
    private ServletContext servletContext;

    @Value("${openapi.config.title:#{null}}")
    private String title;

    @Value("${openapi.config.description:#{null}}")
    private String description;

    @Value("${openapi.config.security.bearerEnabled:false}")
    private Boolean bearerEnabled;

    @Value("${openapi.config.security.clientCredentialsEnabled:false}")
    private Boolean clientCredentialsEnabled;

    @Value("${openapi.config.security.authorizationCodeEnabled:false}")
    private Boolean authorizationCodeEnabled;

    @Value("${openapi.config.security.corsProxyEnabled:false}")
    private Boolean corsProxyEnabled;

    @Value("${openapi.config.security.scopes:offline_access}")
    private String[] scopes;

    @Value("${openapi.config.security.clientScopes:#{null}}")
    private String[] clientScopes;

    @Value("${openapi.config.security.tokenUrl:#{null}}")
    private String tokenUrl;

    @Value("${openapi.config.security.authorizationUrl:#{null}}")
    private String authorizationUrl;

    @Value("${openapi.config.security.refreshUrl:#{null}}")
    private String refreshUrl;

    @Autowired
    private BuildProperties buildProperties;

    @Bean
    public OpenAPI openApi() {
        Components defaultComponents = new Components();
        if (bearerEnabled) {
            defaultComponents.addSecuritySchemes(SecuritySchemeName.BEARER_TOKEN,
                    new SecurityScheme()
                            .name(SecuritySchemeName.BEARER_TOKEN)
                            .description("Bearer Token")
                            .type(SecurityScheme.Type.HTTP)
                            .scheme("bearer")
                            .bearerFormat("JWT")
                            .in(SecurityScheme.In.HEADER)
            );
        }
        if (clientCredentialsEnabled) {
            defaultComponents.addSecuritySchemes(SecuritySchemeName.CLIENT_CREDENTIALS,
                    new SecurityScheme()
                            .name(SecuritySchemeName.CLIENT_CREDENTIALS)
                            .description("Client Credentials")
                            .type(SecurityScheme.Type.OAUTH2)
                            .flows(new OAuthFlows()
                                    .clientCredentials(
                                            new OAuthFlow()
                                                    .tokenUrl(getProxiedUrl(tokenUrl))
                                                    .scopes(getScopes(clientScopes))
                                    )
                            )
            );
        }
        if (authorizationCodeEnabled) {
            defaultComponents.addSecuritySchemes(SecuritySchemeName.AUTHORIZATION_CODE,
                    new SecurityScheme()
                            .name(SecuritySchemeName.AUTHORIZATION_CODE)
                            .description("Authorization Code Flow")
                            .type(SecurityScheme.Type.OAUTH2)
                            .flows(new OAuthFlows()
                                    .authorizationCode(
                                            new OAuthFlow()
                                                    .authorizationUrl(getProxiedUrl(authorizationUrl))
                                                    .tokenUrl(getProxiedUrl(tokenUrl))
                                                    .refreshUrl(getProxiedUrl(refreshUrl))
                                                    .scopes(getScopes(scopes))
                                    )
                            )
            );
        }
        OpenAPI openApi = new OpenAPI().components(new Components())
                .info(new Info().title(title).version(buildProperties.getVersion()).description(description))
                .components(defaultComponents);
        if (bearerEnabled) {
            openApi.addSecurityItem(new SecurityRequirement().addList(SecuritySchemeName.BEARER_TOKEN));
        }
        if (clientCredentialsEnabled) {
            openApi.addSecurityItem(new SecurityRequirement().addList(SecuritySchemeName.CLIENT_CREDENTIALS, Arrays.asList(clientScopes)));
        }
        if (authorizationCodeEnabled) {
            openApi.addSecurityItem(new SecurityRequirement().addList(SecuritySchemeName.AUTHORIZATION_CODE, Arrays.asList(scopes)));
        }
        return openApi;
    }

    @Bean
    @SuppressWarnings("unchecked")
    public OpenApiCustomizer openApiCustomizer() {
        return openApi -> {
            // put error model on response error examples
            openApi.getPaths().values()
                    .forEach(pathItem -> CollectionUtils.emptyIfNull(pathItem.readOperations())
                            .forEach(operation -> operation.getResponses()
                                    .forEach((status, response) -> Optional.ofNullable(response.getContent()).ifPresent(rc -> rc.values()
                                            .forEach(mediaType -> MapUtils.emptyIfNull(mediaType.getExamples())
                                                    .forEach((exampleName, example) -> {
                                                        if (HttpStatus.resolve(Integer.valueOf(status)).isError()) {
                                                            example.setDescription(String.format("Error Code %s: %s", exampleName, example.getSummary()));
                                                            ErrorModel errorModel = new ErrorModel();
                                                            errorModel.setStatus(Integer.valueOf(status));
                                                            errorModel.setCode(exampleName);
                                                            errorModel.setMessage(example.getSummary());
                                                            errorModel.setDetails("Error details");
                                                            example.setValue(errorModel);
                                                        }
                                                    }))))));
            // devires extension to load request examples from classpath
            openApi.getPaths().values()
                    .forEach(pathItem -> CollectionUtils.emptyIfNull(pathItem.readOperations())
                            .forEach(operation -> Optional.ofNullable(operation.getRequestBody()).ifPresent(op -> op.getContent().values()
                                    .forEach(mediaType -> MapUtils.emptyIfNull(mediaType.getExamples())
                                            .forEach((exampleName, example) -> MapUtils.emptyIfNull(example.getExtensions())
                                                    .forEach((extensionName, extension) -> {
                                                        if (extensionName.endsWith(DeviresExtensionConstants.EXTENSION_NAME)) {
                                                            HashMap<String, String> props = (HashMap) extension;
                                                            String exampleValue = ResourceUtils.readString(props.get(DeviresExtensionConstants.PROP_CLASSPATH_EXAMPLE));
                                                            example.setValue(exampleValue);
                                                        }
                                                    })
                                            )))));
            // devires extension to load response examples from classpath
            openApi.getPaths().values()
                    .forEach(pathItem -> CollectionUtils.emptyIfNull(pathItem.readOperations())
                            .forEach(operation -> operation.getResponses()
                                    .forEach((status, response) -> Optional.ofNullable(response.getContent()).ifPresent(rc -> rc.values()
                                            .forEach(mediaType -> MapUtils.emptyIfNull(mediaType.getExamples())
                                                    .forEach((exampleName, example) -> MapUtils.emptyIfNull(example.getExtensions())
                                                            .forEach((extensionName, extension) -> {
                                                                if (extensionName.endsWith(DeviresExtensionConstants.EXTENSION_NAME)) {
                                                                    HashMap<String, String> props = (HashMap) extension;
                                                                    String exampleValue = ResourceUtils.readString(props.get(DeviresExtensionConstants.PROP_CLASSPATH_EXAMPLE));
                                                                    example.setValue(exampleValue);
                                                                }
                                                            })
                                                    ))))));
        };
    }

    @Bean
    public ServletRegistrationBean<HttpServlet> corsProxyServlet() {
        ServletRegistrationBean<HttpServlet> servRegBean = new ServletRegistrationBean<>();
        servRegBean.setServlet(new SwaggerUICorsProxyServlet());
        servRegBean.addUrlMappings(PROXY_SERVLET_URI);
        servRegBean.setOrder(0);
        servRegBean.setLoadOnStartup(1);
        return servRegBean;
    }

    private String getProxiedUrl(String targetUrl) {
        if (corsProxyEnabled && StringUtils.hasText(targetUrl)) {
            return servletContext.getContextPath() + PROXY_SERVLET_URI + "?" + SwaggerUICorsProxyServlet.PROXY_URL_PARAM_NAME + "=" + targetUrl;
        }
        return targetUrl;
    }

    private Scopes getScopes(String[] scopeArray) {
        Scopes scopes = null;
        if (ArrayUtils.isNotEmpty(scopeArray)) {
            scopes = new Scopes();
            for (String scp : scopeArray) {
                scopes.addString(scp, scp);
            }
        }
        return scopes;
    }

}
