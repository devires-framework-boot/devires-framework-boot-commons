package br.com.devires.framework.boot.commons.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PaginationResponse<T> {

    @Schema(description = "Page content")
    private List<T> content;

    @Schema(description = "Pagination info")
    private PaginationInfo pagination;

}
