package br.com.devires.framework.boot.commons.openapi;

public class DeviresExtensionConstants {

    public static final String EXTENSION_NAME = "devires";
    public static final String PROP_CLASSPATH_EXAMPLE = "classpathExample";

}
