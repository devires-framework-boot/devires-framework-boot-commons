package br.com.devires.framework.boot.commons.model;

import org.springframework.http.HttpStatus;

public enum CommonErrors {

    INVALID_PARAMETERS (HttpStatus.BAD_REQUEST, Constants.INVALID_PARAMETERS_CODE, Constants.INVALID_PARAMETERS_MSG),
    UNAUTHORIZED (HttpStatus.UNAUTHORIZED, Constants.UNAUTHORIZED_CODE, Constants.UNAUTHORIZED_MSG),
    UNEXPECTED_ERROR (HttpStatus.INTERNAL_SERVER_ERROR, Constants.UNEXPECTED_ERROR_CODE, Constants.UNEXPECTED_ERROR_MSG);

    public static class Constants {
        public static final String INVALID_PARAMETERS_CODE = "400000";
        public static final String INVALID_PARAMETERS_MSG = "Invalid parameters";
        public static final String UNAUTHORIZED_CODE = "401000";
        public static final String UNAUTHORIZED_MSG = "Unauthorized";
        public static final String UNEXPECTED_ERROR_CODE = "500000";
        public static final String UNEXPECTED_ERROR_MSG = "Unexpected error";
    }

    private ErrorModel model;

    CommonErrors (HttpStatus status, String code, String msg) {
        model = new ErrorModel(status.value(), code, msg);
    }

    public ErrorModel get() {
        return model;
    }

    public ErrorModel get(String details) {
        return model.setDetails(details);
    }

}
