package br.com.devires.framework.boot.commons.jaeger;

import io.jaegertracing.internal.JaegerSpan;
import io.jaegertracing.spi.Reporter;
import org.slf4j.MDC;

public class JaegerMDCReporter implements Reporter {

    private static final String MDC_TRACE = "trace";
    private static final String SEPARATOR = ",";

    @Override
    public void report(JaegerSpan span) {
        MDC.put(MDC_TRACE, SEPARATOR + span.context().toString());
    }

    @Override
    public void close() {
        // empty
    }

}
