package br.com.devires.framework.boot.commons.servlet;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Map;

public class SwaggerUICorsProxyServlet extends HttpServlet {
    
    public static final String PROXY_URL_PARAM_NAME = "_proxyTo";

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String proxyTo = request.getParameter(PROXY_URL_PARAM_NAME);
        Assert.hasText(proxyTo, PROXY_URL_PARAM_NAME + " required missing");
        HttpHeaders headersToPass = new HttpHeaders();
        headersToPass.add(HttpHeaders.CONTENT_TYPE, request.getContentType());
        headersToPass.add(HttpHeaders.ACCEPT, request.getHeader(HttpHeaders.ACCEPT));
        if (request.getHeader(HttpHeaders.AUTHORIZATION) != null) {
            headersToPass.add(HttpHeaders.AUTHORIZATION, request.getHeader(HttpHeaders.AUTHORIZATION));
        }
        MultiValueMap<String, String> callParams = new LinkedMultiValueMap<>();
        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            if (!entry.getKey().equalsIgnoreCase(PROXY_URL_PARAM_NAME)) {
                callParams.add(entry.getKey(), StringUtils.arrayToCommaDelimitedString(entry.getValue()));
            }
        }
        HttpEntity<MultiValueMap<String, String>> callEntity = new HttpEntity<>(callParams, headersToPass);
        ResponseEntity<String> callResponse;
        try {
            callResponse = new RestTemplate().postForEntity(proxyTo, callEntity, String.class);
        } catch (HttpStatusCodeException e) {
            callResponse = ResponseEntity.status(e.getRawStatusCode()).headers(e.getResponseHeaders()).body(e.getResponseBodyAsString());
        }
        response.setStatus(callResponse.getStatusCode().value());
        response.setContentType(callResponse.getHeaders().getContentType().toString());
        response.getWriter().write(callResponse.getBody());
        response.flushBuffer();
    }

}
