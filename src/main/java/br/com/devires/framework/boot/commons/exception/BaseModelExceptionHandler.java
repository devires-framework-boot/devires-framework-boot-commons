package br.com.devires.framework.boot.commons.exception;

import br.com.devires.framework.boot.commons.model.CommonErrors;
import br.com.devires.framework.boot.commons.model.ErrorModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;

import java.util.Iterator;

@Slf4j
public abstract class BaseModelExceptionHandler {

    @ExceptionHandler(ModelException.class)
    public ResponseEntity<Object> handleModelException(ModelException ex, WebRequest request) {
        return new ResponseEntity<>(ex.getModel(), HttpStatus.resolve(ex.getModel().getStatus()));
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<Object> handleResponseStatusException(ResponseStatusException ex, WebRequest request) {
        log.error("Error occurred", ex);
        HttpStatusCode status = ex.getStatusCode();
        ErrorModel errorModel = new ErrorModel(status.value(), status.value() + "000", ex.getReason());
        return new ResponseEntity<>(errorModel, status);
    }

    @ExceptionHandler(HttpMediaTypeException.class)
    protected ResponseEntity<Object> handleHttpMediaTypeException(HttpMediaTypeException ex, WebRequest request) {
        ErrorModel errorModel = CommonErrors.INVALID_PARAMETERS.get("Unsupported Media Type");
        return new ResponseEntity<>(errorModel, HttpStatus.resolve(errorModel.getStatus()));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, WebRequest request) {
        ErrorModel errorModel = CommonErrors.INVALID_PARAMETERS.get("Required request body is missing");
        return new ResponseEntity<>(errorModel, HttpStatus.resolve(errorModel.getStatus()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, WebRequest request) {
        StringBuffer sb = new StringBuffer();
        Iterator<ObjectError> errorsIterator = ex.getBindingResult().getAllErrors().iterator();
        while(errorsIterator.hasNext()) {
            ObjectError error = errorsIterator.next();
            String field = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            sb.append(String.format("%s: %s", field, message));
            if (errorsIterator.hasNext()) {
                sb.append(", ");
            }
        }
        ErrorModel errorModel = CommonErrors.INVALID_PARAMETERS.get(sb.toString());
        return new ResponseEntity<>(errorModel, HttpStatus.resolve(errorModel.getStatus()));
    }

    @ExceptionHandler(BindException.class)
    protected ResponseEntity<Object> handleBindException(BindException ex, WebRequest request) {
        StringBuffer sb = new StringBuffer();
        Iterator<ObjectError> errorsIterator = ex.getBindingResult().getAllErrors().iterator();
        while(errorsIterator.hasNext()) {
            ObjectError error = errorsIterator.next();
            String field = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            sb.append(String.format("%s: %s", field, message));
            if (errorsIterator.hasNext()) {
                sb.append(", ");
            }
        }
        ErrorModel errorModel = CommonErrors.INVALID_PARAMETERS.get(sb.toString());
        return new ResponseEntity<>(errorModel, HttpStatus.resolve(errorModel.getStatus()));
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        ErrorModel errorModel = CommonErrors.UNAUTHORIZED.get();
        return new ResponseEntity<>(errorModel, HttpStatus.resolve(errorModel.getStatus()));
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException ex, WebRequest request) {
        ErrorModel errorModel = CommonErrors.UNAUTHORIZED.get();
        return new ResponseEntity<>(errorModel, HttpStatus.resolve(errorModel.getStatus()));
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleUnknownException(Exception ex, WebRequest request) {
        log.error("Unexpected error occurred", ex);
        ErrorModel errorModel = CommonErrors.UNEXPECTED_ERROR.get();
        return new ResponseEntity<>(errorModel, HttpStatus.resolve(errorModel.getStatus()));
    }

}
