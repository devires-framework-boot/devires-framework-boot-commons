package br.com.devires.framework.boot.commons.utils;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

public class HibernateUtils {

    @SuppressWarnings("unchecked")
    public static <T> T initializeAndUnproxy(T entity) {
        if (entity != null && entity instanceof HibernateProxy) {
            Hibernate.initialize(entity);
            entity = (T) ((HibernateProxy) entity).getHibernateLazyInitializer().getImplementation();
        }
        return entity;
    }

    public static <T> T deepNullifyProxies(T entity) {
        Set<Object> dejaVu = Collections.newSetFromMap(new IdentityHashMap<>());
        try {
            deepNullifyProxies(entity, dejaVu);
        } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        return entity;
    }

    private static void deepNullifyProxies(Object entity, Set<Object> dejaVu) throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        if (entity == null || dejaVu.contains(entity)) {
            return;
        } else {
            dejaVu.add(entity);
            PropertyDescriptor[] properties = Introspector.getBeanInfo(entity.getClass(), Object.class).getPropertyDescriptors();
            for (PropertyDescriptor propertyDescriptor : properties) {
                Object prop;
                prop = propertyDescriptor.getReadMethod().invoke(entity);
                if (prop != null) {
                    if (!Hibernate.isInitialized(prop)) {
                        propertyDescriptor.getWriteMethod().invoke(entity, new Object[]{null});
                    } else if (prop instanceof Collection) {
                        for (Object item : (Collection<?>) prop) {
                            deepNullifyProxies(item, dejaVu);
                        }
                    } else {
                        deepNullifyProxies(prop, dejaVu);
                    }
                }
            }
        }
    }


}
