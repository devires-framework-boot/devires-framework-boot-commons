package br.com.devires.framework.boot.commons.jaeger;

import io.jaegertracing.spi.Reporter;
import io.opentracing.contrib.java.spring.jaeger.starter.ReporterAppender;

import java.util.Collection;

public class JaegerReportAppender implements ReporterAppender {

    @Override
    public void append(Collection<Reporter> reporters) {
        reporters.add(new JaegerMDCReporter());
    }

}
