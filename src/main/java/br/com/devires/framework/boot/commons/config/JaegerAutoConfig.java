package br.com.devires.framework.boot.commons.config;

import br.com.devires.framework.boot.commons.jaeger.JaegerReportAppender;
import io.jaegertracing.spi.Reporter;
import io.opentracing.contrib.java.spring.jaeger.starter.ReporterAppender;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass({ReporterAppender.class, Reporter.class})
public class JaegerAutoConfig {

    @Bean
    public JaegerReportAppender customLoggingReporter() {
        return new JaegerReportAppender();
    }

}
