package br.com.devires.framework.boot.commons.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ISOLanguageValidator.class)
public @interface ISOLanguage {
    String message() default "must be a valid ISO language code";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
