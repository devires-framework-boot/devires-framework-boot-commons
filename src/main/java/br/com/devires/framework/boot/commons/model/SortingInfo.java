package br.com.devires.framework.boot.commons.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SortingInfo {

    @Schema(description = "Sorting field", example = "name")
    private String field;

    @Schema(description = "Sorting direction, one of: asc, desc", example = "asc")
    private String direction;

}
