package br.com.devires.framework.boot.commons.config;

import br.com.devires.framework.boot.commons.redis.RedisCacheErrorHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCache;

@Configuration
@ConditionalOnClass(RedisCache.class)
public class RedisAutoConfig extends CachingConfigurerSupport implements CachingConfigurer {

    @Override
    public CacheErrorHandler errorHandler() {
        return new RedisCacheErrorHandler();
    }

}
