package br.com.devires.framework.boot.commons.config;

import br.com.devires.framework.boot.commons.loadbalancer.StaticServiceInstanceListSupplier;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
@ConfigurationProperties("loadbalancer")
@ConditionalOnProperty(name = "loadbalancer.enabled", havingValue = "true")
@ConditionalOnClass({LoadBalancerClient.class, ServiceInstanceListSupplier.class})
public class LoadBalancerAutoConfig {

    @Setter
    private Map<String, Map<String, Object>> instances;

    @Bean
    public ServiceInstanceListSupplier serviceInstanceListSupplier() {
        return new StaticServiceInstanceListSupplier(instances);
    }

}
