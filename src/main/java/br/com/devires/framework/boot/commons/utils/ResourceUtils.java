package br.com.devires.framework.boot.commons.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

public class ResourceUtils {

    public static Resource read(String path) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        return resourceLoader.getResource(path);
    }

    public static String readString(String path) {
        Resource resource = read(path);
        try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static byte[] readBytes(String path) {
        Resource resource = read(path);
        try (InputStream in = resource.getInputStream()) {
            return FileCopyUtils.copyToByteArray(in);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void readLines(String resourcePath, Consumer<String> lineConsumer) {
        try (Reader reader = new InputStreamReader(new ClassPathResource(resourcePath).getInputStream(), StandardCharsets.UTF_8)) {
            BufferedReader br = new BufferedReader(reader);
            String line = null;
            while ((line = br.readLine()) != null) {
                lineConsumer.accept(line);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Error reading resource", e);
        }
    }

}
