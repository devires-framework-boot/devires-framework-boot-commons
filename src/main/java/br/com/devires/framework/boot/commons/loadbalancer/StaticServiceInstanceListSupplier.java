package br.com.devires.framework.boot.commons.loadbalancer;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import reactor.core.publisher.Flux;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class StaticServiceInstanceListSupplier implements ServiceInstanceListSupplier {

    private List<ServiceInstance> serviceInstances;

    public StaticServiceInstanceListSupplier(Map<String, Map<String, Object>> instanceConfigs) {
        serviceInstances = new ArrayList<>();
        if (MapUtils.isNotEmpty(instanceConfigs)) {
            for (Map.Entry<String, Map<String, Object>> instanceEntry : instanceConfigs.entrySet()) {
                String serviceId = instanceEntry.getKey();
                Map<String, Object> props = instanceEntry.getValue();
                if (MapUtils.isNotEmpty(props) && props.containsKey("servers")) {
                    String servers = (String) props.get("servers");
                    List<String> serverList = Arrays.asList(servers.split(","));
                    if (CollectionUtils.isNotEmpty(serverList)) {
                        for (int i = 0; i < serverList.size(); i++) {
                            String server = serverList.get(i);
                            String instanceId = server + i;
                            DefaultServiceInstance serviceInstance;
                            if (server.startsWith("http")) {
                                serviceInstance = new DefaultServiceInstance();
                                serviceInstance.setUri(URI.create(server));
                            } else {
                                String[] hostAndPort = server.split(":");
                                String host = hostAndPort[0];
                                Integer port = hostAndPort.length > 1 ? Integer.valueOf(hostAndPort[1]) : 80;
                                Boolean secure = port == 443 ? true : false;
                                serviceInstance = new DefaultServiceInstance(instanceId, serviceId, host, port, secure);
                            }
                            serviceInstances.add(serviceInstance);
                        }
                    }
                }
            }
        }
    }

    @Override
    public String getServiceId() {
        return StaticServiceInstanceListSupplier.class.getName();
    }

    @Override
    public Flux<List<ServiceInstance>> get() {
        return Flux.just(serviceInstances);
    }

}
