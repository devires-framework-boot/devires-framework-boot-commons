package br.com.devires.framework.boot.commons.utils;

import org.mapstruct.Named;

import java.util.Optional;

public class MapStructUtils {

    @Named("fromOptional")
    public static <T> T fromOptional(Optional<T> optional) {
        if (optional == null) {
            return null;
        }
        return optional.orElse(null);
    }

}
