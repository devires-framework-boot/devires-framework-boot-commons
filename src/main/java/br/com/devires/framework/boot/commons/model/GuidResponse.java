package br.com.devires.framework.boot.commons.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuidResponse {

    @Schema(description = "Unique identifier guid", example = "6eb42673-b3ca-48d9-95f0-32e4874665e2")
    private String guid;

}
