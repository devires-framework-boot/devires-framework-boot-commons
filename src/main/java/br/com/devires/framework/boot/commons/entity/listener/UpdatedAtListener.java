package br.com.devires.framework.boot.commons.entity.listener;

import jakarta.persistence.PreUpdate;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UpdatedAtListener {

    @PreUpdate
    public void preUpdateForUpdateAuditInfo(final Updatable entity) {
        entity.setUpdatedAt(new Date());
    }

}
