package br.com.devires.framework.boot.commons.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class ErrorModel {

    @NonNull
    @Schema(description = "Http status code", example = "400", required = true)
    private Integer status;

    @NonNull
    @Schema(description = "Error code", example = "400001", required = true)
    private String code;

    @NonNull
    @Schema(description = "Error message", example = "Error message", required = true)
    private String message;

    @Schema(description = "Error details", example = "Error details")
    private String details;

    public ErrorModel setDetails(String details) {
        this.details = details;
        return this;
    }

}
