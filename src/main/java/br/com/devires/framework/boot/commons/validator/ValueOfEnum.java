package br.com.devires.framework.boot.commons.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValueOfEnumValidator.class)
public @interface ValueOfEnum {
    Class<? extends Enum<?>> enumClazz();
    String message() default "must be any of ${acceptedValues}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
