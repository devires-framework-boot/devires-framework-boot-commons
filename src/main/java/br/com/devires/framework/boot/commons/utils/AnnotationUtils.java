package br.com.devires.framework.boot.commons.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class AnnotationUtils {

    public static boolean containsAnnotation(Object object, Class<? extends Annotation> annotation) {
        Class<?> clazz = object.getClass();
        do {
            if (clazz.getAnnotation(annotation) != null) {
                return true;
            }
        } while ((clazz = clazz.getSuperclass()) != null);
        return false;
    }

    public static boolean containsAnnotation(Field field, Class<? extends Annotation> annotation) {
        return field.getAnnotation(annotation) != null;
    }

    public static <T extends Annotation> T getAnnotation(Object object, Class<T> annotation) {
        Class<?> clazz = object.getClass();
        do {
            if (clazz.getAnnotation(annotation) != null) {
                return clazz.getAnnotation(annotation);
            }
        } while ((clazz = clazz.getSuperclass()) != null);
        return null;
    }

}
