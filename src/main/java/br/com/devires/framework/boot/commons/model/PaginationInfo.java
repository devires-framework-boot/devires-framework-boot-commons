package br.com.devires.framework.boot.commons.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class PaginationInfo {

    @Schema(description = "Page number, starting at 0", example = "0")
    private Integer pageNumber;

    @Schema(description = "Page size", example = "20")
    private Integer pageSize;

    @Schema(description = "Content size", example = "20")
    private Integer contentSize;

    @Schema(description = "Total elements", example = "100")
    private Long totalElements;

    @Schema(description = "Total pages", example = "5")
    private Integer totalPages;

    @Schema(description = "Has content", example = "true")
    private Boolean hasContent;

    @Schema(description = "Has previous page", example = "false")
    private Boolean hasPreviousPage;

    @Schema(description = "Has next page", example = "true")
    private Boolean hasNextPage;

    @Schema(description = "Sorting info")
    private List<SortingInfo> sorting;

}
