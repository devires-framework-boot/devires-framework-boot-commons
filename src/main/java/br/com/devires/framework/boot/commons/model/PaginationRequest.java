package br.com.devires.framework.boot.commons.model;

import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.Optional;

@Data
public class PaginationRequest<T> {

    @Parameter(description = "Page number, starting at 0", example = "0")
    @NotNull
    private Integer pageNumber;

    @Parameter(description = "Page size", example = "20")
    @NotNull
    private Integer pageSize;

    @Parameter(description = "Sort by field", example = "displayName")
    private Optional<@NotBlank String> sortBy;

    @Parameter(description = "Sort direction, one of: asc, desc", example = "asc")
    private Optional<@NotBlank String> sortDirection;

}
