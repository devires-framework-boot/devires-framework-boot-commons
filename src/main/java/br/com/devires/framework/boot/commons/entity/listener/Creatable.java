package br.com.devires.framework.boot.commons.entity.listener;

import java.util.Date;

public interface Creatable {

    void setCreatedAt(final Date createdAt);

}
